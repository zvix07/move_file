﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.IO;

namespace Move_Files.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_is_file_move()
        {
            string path_folder = @"C:\Users\volod\Desktop\Some files";
            string path_file_copy = path_folder + @"/Files copy";
            int expected_number_of_files = 54;

            Move_files move_Files = new Move_files();
            move_Files.Move_files_from_directory(path_folder);

            int actual_number_of_files = Directory.GetFiles(path_file_copy, "*.*", SearchOption.AllDirectories).Length;

            Assert.AreEqual(expected_number_of_files, actual_number_of_files);
        }
        
    }
}
