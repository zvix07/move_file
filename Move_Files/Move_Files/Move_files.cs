﻿using System;
using System.IO;

namespace Move_Files
{
    public class Move_files
    {
          public void Move_files_from_directory(string path_to_folder)
        {
            var list_of_files = Directory.GetFiles(path_to_folder, "*.*", SearchOption.AllDirectories);
            var list_of_directories = Directory.GetDirectories(path_to_folder, "*.*", SearchOption.AllDirectories);
           
            string path_files_copy = path_to_folder + "//Files copy";
            Directory.CreateDirectory(path_files_copy);
            foreach (var item in list_of_files)
            {
                string new_path = path_files_copy + "//" + Path.GetFileName(item);
                File.Move(item, new_path);   
            }
            foreach (var item in list_of_directories)
            {
                Directory.Delete(item);
            }
        }
    }
}
